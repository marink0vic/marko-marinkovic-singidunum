package marko.marinkovic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import marko.marinkovic.singidunum.entities.City;
import marko.marinkovic.singidunum.entities.Subject;

@Repository
public class CityDao implements SchoolCrudRepository<City> {

	private SessionFactory sessionFactory;

	@Autowired
	public CityDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@Override
	public void save(City city) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(city);
	}

	@Override
	public List<City> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<City> query = session.createQuery("from City", City.class);
		return query.getResultList();
	}

	@Override
	public City findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}
}

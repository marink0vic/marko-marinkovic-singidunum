package marko.marinkovic.singidunum.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import marko.marinkovic.singidunum.entities.Subject;

@Repository
public class SubjectDao implements SchoolCrudRepository<Subject> {

	private SessionFactory sessionFactory;

	@Autowired
	public SubjectDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void save(Subject subject) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(subject);
	}
	
	@Override
	public List<Subject> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<Subject> query = session.createQuery("from Subject", Subject.class);
		return query.getResultList();
	}
	
	@Override
	public Subject findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Subject.class, id);
	}
	
	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		Subject subject = findById(id);
		session.delete(subject);
	}
	
	public Subject fetchStudents(int id) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Subject> query = session.createNamedQuery("fetchStudents", Subject.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}
	
	public Subject fetchProfessors(int id) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Subject> query = session.createNamedQuery("fetchProfessors", Subject.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}
}

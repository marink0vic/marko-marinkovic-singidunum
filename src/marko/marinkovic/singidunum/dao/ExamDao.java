package marko.marinkovic.singidunum.dao;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import marko.marinkovic.singidunum.entities.Exam;

@Repository
public class ExamDao implements SchoolCrudRepository<Exam>, PaginationRepository<Exam> {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public ExamDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Exam exam) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(exam);
	}

	@Override
	public List<Exam> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<Exam> query = session.createQuery("from Exam", Exam.class);
		return query.getResultList();
	}

	@Override
	public Exam findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Exam> query = session.createNamedQuery("fetchStudentsForExam", Exam.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(findById(id));
	}
	
	@Override
	public List<Exam> findAllWithLimit(int limit) {
		Session session = sessionFactory.getCurrentSession();
		Query<Exam> query = session.createQuery("from Exam", Exam.class);
		query.setFirstResult(0);
		query.setMaxResults(limit);
		return query.getResultList();
	}

	public List<Exam> findActiveForSubject(Integer subjectID, LocalDate examDate) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Exam> query = session.createNamedQuery("fetchActiveExams", Exam.class);
		query.setParameter("id", subjectID);
		query.setParameter("date", examDate);
		return query.getResultList();
	}

}

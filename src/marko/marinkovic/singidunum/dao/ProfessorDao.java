package marko.marinkovic.singidunum.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import marko.marinkovic.singidunum.entities.Professor;


@Repository
public class ProfessorDao implements SchoolCrudRepository<Professor> {

	private SessionFactory sessionFactory;

	@Autowired
	public ProfessorDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Professor professor) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(professor);
	}

	@Override
	public List<Professor> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<Professor> query = session.createQuery("from Professor", Professor.class);
		return query.getResultList();
	}

	@Override
	public Professor findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Professor> query = session.createNamedQuery("fetchProfessorSubjects", Professor.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(findById(id));
	}
	
	public Professor getActiveProfessor(int id, String username) {
		Session session = sessionFactory.getCurrentSession();
		Query<Professor> query = session.createQuery("from Professor p where p.email=:username and p.id!=:id", Professor.class);
		query.setParameter("username", username);
		query.setParameter("id", id);
		return query.getResultList().stream().findFirst().orElse(null);
	}

	public Professor findByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		Query<Professor> query = session.createQuery("from Professor p where p.email=:email", Professor.class);
		query.setParameter("email", email);
		return query.getResultList().stream().findFirst().orElse(null);
	}
}

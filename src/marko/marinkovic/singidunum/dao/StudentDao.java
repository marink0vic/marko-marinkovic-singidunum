package marko.marinkovic.singidunum.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import marko.marinkovic.singidunum.entities.Student;

@Repository
public class StudentDao implements SchoolCrudRepository<Student>, PaginationRepository<Student> {
	
	private SessionFactory sessionFactory; 
	
	@Autowired
	public StudentDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Student student) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(student);
	}

	@Override
	public List<Student> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<Student> query = session.createQuery("from Student", Student.class);
		return query.getResultList();
	}

	@Override
	public Student findById(int id) {
		return namedQueryResult(id, "fetchStudentSubjects");
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(findById(id));
	}
	
	@Override
	public List<Student> findAllWithLimit(int limit) {
		Session session = sessionFactory.getCurrentSession();
		Query<Student> query = session.createQuery("from Student", Student.class);
		query.setFirstResult(0);
		query.setMaxResults(limit);
		return query.getResultList();
	}
	
	public Student fetchExams(int id) {
		return namedQueryResult(id, "fetchStudentExams");
	}
	
	public Student fetchAll(int id) {
		return namedQueryResult(id, "fetchAll");
	}
	
	public Student namedQueryResult(int id, String queryName) {
		Session session = sessionFactory.getCurrentSession();
		TypedQuery<Student> query = session.createNamedQuery(queryName, Student.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	public Student getActiveStudent(int id, String username, String index) {
		Session session = sessionFactory.getCurrentSession();
		Query<Student> query = session.createQuery("from Student s where (s.email=:username or s.indexNumber=:index) and s.id!=:id", Student.class);
		query.setParameter("username", username);
		query.setParameter("index", index);
		query.setParameter("id", id);
		return query.getResultList().stream().findFirst().orElse(null);
	}
	
}

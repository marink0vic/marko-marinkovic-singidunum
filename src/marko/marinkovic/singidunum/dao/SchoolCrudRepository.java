package marko.marinkovic.singidunum.dao;

import java.util.List;

public interface SchoolCrudRepository<T> {
	void save(T object);
	List<T> findAll();
	T findById(int id);
	void delete(int id);
}

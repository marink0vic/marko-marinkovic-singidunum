package marko.marinkovic.singidunum.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import marko.marinkovic.singidunum.entities.Title;


@Repository
public class TitleDao implements SchoolCrudRepository<Title>{

	private SessionFactory sessionFactory;

	@Autowired
	public TitleDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Title title) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Title> findAll() {
		Session session = sessionFactory.getCurrentSession();
		Query<Title> query = session.createQuery("from Title", Title.class);
		return query.getResultList();
	}

	@Override
	public Title findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}
	
}

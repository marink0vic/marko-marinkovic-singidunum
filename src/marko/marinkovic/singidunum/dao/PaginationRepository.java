package marko.marinkovic.singidunum.dao;

import java.util.List;

public interface PaginationRepository<T> {
	List<T> findAllWithLimit(int limit);
}

package marko.marinkovic.singidunum.constants;

public enum ExamStatus {
	ACTIVE, FINISHED
}

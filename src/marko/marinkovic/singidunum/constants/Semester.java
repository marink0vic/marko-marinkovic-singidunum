package marko.marinkovic.singidunum.constants;

public enum Semester {
	SUMMER, WINTER
}

package marko.marinkovic.singidunum.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NamedQueries(value = {
		@NamedQuery(name = "fetchStudentSubjects", query = "select s from Student s left join fetch s.subjects where s.id=:id"),
		@NamedQuery(name = "fetchStudentExams", query = "select s from Student s left join fetch s.exams where s.id=:id"),
		@NamedQuery(name = "fetchAll", query = "select s from Student s left join fetch s.subjects left join fetch s.exams where s.id=:id")})
public class Student extends User {
	
	@Column(name="index_number", length = 10, nullable = false, unique = true)
	@Size(min=10, max=10, message="Index must contains 10 characters")
	@NotNull
	private String indexNumber;
	
	@Column(name="current_year_of_study", length = 7, nullable = false)
	private int currentStudyYear;
	
	@ManyToMany
	@JoinTable(name="subject_student",
	joinColumns = @JoinColumn(name="student_id"),
	inverseJoinColumns = @JoinColumn(name="subject_id"))
	private Set<Subject> subjects = new HashSet<>();

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name="exam_student",
	joinColumns = @JoinColumn(name="student_id"),
	inverseJoinColumns = @JoinColumn(name="exam_id"))
	private Set<Exam> exams = new HashSet<>();
	
	public Student() {
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public int getCurrentStudyYear() {
		return currentStudyYear;
	}

	public void setCurrentStudyYear(int currentStudyYear) {
		this.currentStudyYear = currentStudyYear;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	public void addSubject(Subject sub) {
		if (sub != null) 
			subjects.add(sub);
		sub.addStudent(this);
	}

	public void addExam(Exam exam) {
		if (exam != null)
			exams.add(exam);
		exam.addStudent(this);
	}
	
}

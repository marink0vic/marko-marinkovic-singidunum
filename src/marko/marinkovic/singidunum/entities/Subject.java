package marko.marinkovic.singidunum.entities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import marko.marinkovic.singidunum.constants.Semester;

@Entity
@NamedQueries(value = {
		@NamedQuery(name = "fetchStudents", query = "select s from Subject s left join fetch s.students where s.subjectID=:id"),
		@NamedQuery(name = "fetchProfessors", query = "select s from Subject s left join fetch s.professors where s.subjectID=:id")})
public class Subject {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="subject_id")
	private Integer subjectID;
	
	@Column(length = 30, nullable = false)
	@Size(min=3, message="Minimal characters subject name is 3")
	private String name;
	
	@Column(length = 200)
	@Size(max=200, message="Max characters for description is 200")
	private String description;
	
	@Column(name="year_of_study", length = 1)
	private int yearOfStudy;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 10)
	private Semester semester;
	
	@OneToMany(mappedBy = "subject")
	private List<Exam> exams;
	
	@ManyToMany(mappedBy = "subjects")
	private Set<Student> students = new HashSet<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="professor_subject",
	joinColumns = @JoinColumn(name="subject_id"),
	inverseJoinColumns = @JoinColumn(name="professor_id"))
	private Set<Professor> professors = new HashSet<>();

	public Integer getSubjectID() {
		return subjectID;
	}

	public void setSubjectID(Integer subjectID) {
		this.subjectID = subjectID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}
	
	public void addProfessor(Professor professor) {
		if (professor != null)
			professors.add(professor);
	}
	
	public void addStudent(Student student) {
		if (student != null)
			students.add(student);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((semester == null) ? 0 : semester.hashCode());
		result = prime * result + ((subjectID == null) ? 0 : subjectID.hashCode());
		result = prime * result + yearOfStudy;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (semester != other.semester)
			return false;
		if (subjectID == null) {
			if (other.subjectID != null)
				return false;
		} else if (!subjectID.equals(other.subjectID))
			return false;
		if (yearOfStudy != other.yearOfStudy)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Subject [subjectID=" + subjectID + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}
	
}

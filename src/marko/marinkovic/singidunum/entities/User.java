package marko.marinkovic.singidunum.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="first_name", length = 30, nullable = false)
	@Size(min=3, message="Minimal characters for first name is 3")
	@NotNull
	private String firstName;
	
	@Column(name="last_name", length = 30, nullable = false)
	@Size(min=3, message="Minimal characters for last name is 3")
	@NotNull
	private String lastName;
	
	@Column(name="username", length = 30, nullable = false, unique = true)
	@Email(message = "this is not a valid email address")
	@NotNull
	private String email;
	
	@Column(nullable = false)
	@Size(min=3, message="Minimal characters for password is 3")
	@NotNull
	private String password;
	
	@Column(length = 50)
	@Size(min=3, message="Minimal characters for address is 3")
	private String address;
	
	@Column(length = 15)
	@Size(min=3, message="Minimal characters for phone is 3")
	private String phone;
	
	@ManyToOne
	@JoinColumn(name="city_id")
	private City city;
	
	public User () {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}
	
}

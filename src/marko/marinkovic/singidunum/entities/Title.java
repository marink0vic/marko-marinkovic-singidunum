package marko.marinkovic.singidunum.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Title {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="title_id", length = 7)
	private Integer titleId;
	private String name;
	
	public Title() {
		
	}

	public Integer getTitleId() {
		return titleId;
	}

	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

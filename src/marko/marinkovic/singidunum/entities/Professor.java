package marko.marinkovic.singidunum.entities;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@NamedQueries(value = {
		@NamedQuery(name = "fetchProfessorSubjects", query = "select p from Professor p left join fetch p.subjects where p.id=:id")})
public class Professor extends User implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Column(name="reelection_date", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull
	@Future
	private LocalDate reelectionDate;
	
	@ManyToOne
	@JoinColumn(name="title_id", nullable = false)
	private Title title;
	
	@OneToMany(mappedBy = "professor", cascade = CascadeType.ALL)
	private List<Exam> exams;
	
	@ManyToMany(mappedBy = "professors")
	private Set<Subject> subjects = new HashSet<>();
	
	public Professor() {
		
	}

	public LocalDate getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(LocalDate reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	public void addSubject(Subject sub) {
		if (sub != null)
			subjects.add(sub);
		sub.addProfessor(this);
	}

	@Override
	public String toString() {
		return super.toString() + " Professor [reelectionDate=" + reelectionDate + ", title=" + title + "]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_PROFESSOR"));
	}

	@Override
	public String getUsername() {
		return super.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	
}

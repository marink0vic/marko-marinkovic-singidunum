package marko.marinkovic.singidunum.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import marko.marinkovic.singidunum.entities.City;
import marko.marinkovic.singidunum.entities.Student;
import marko.marinkovic.singidunum.entities.Subject;
import marko.marinkovic.singidunum.services.CityService;
import marko.marinkovic.singidunum.services.StudentService;
import marko.marinkovic.singidunum.services.SubjectService;

@Controller
@RequestMapping("/students")
public class StudentController {

	private StudentService studentService;
	private SubjectService subjectService;
	private CityService cityService;
	private List<City> cities;
	
	@Autowired
	public StudentController(StudentService studentService, SubjectService subjectService, CityService cityService) {
		this.studentService = studentService;
		this.subjectService = subjectService;
		this.cityService = cityService;
		this.cities = cityService.findAll();
	}
	
	@GetMapping("")
	public String getStudents(Model model) {
		List<Student> students = studentService.findAll();
		model.addAttribute("students", students);
		return "students";
	}
	
	@PostMapping("")
	public String getStudentsWithLimit(Model model, HttpServletRequest request) {
		int size = Integer.parseInt(request.getParameter("studentNumber"));
		List<Student> students = studentService.findAllWithLimit(size);
		model.addAttribute("students", students);
		return "students";
	}
	
	@GetMapping("/{studentId}")
	public String getStudent(@PathVariable Integer studentId, Model model) {
		Student student = studentService.fetchAll(studentId);
		model.addAttribute("student", student);
		return "student-details";
	}
	
	@GetMapping("/addStudentForm")
	public String showStudentForm(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		model.addAttribute("cities", cities);
		return "student-form";
	}
	
	@PostMapping("/saveStudent")
	public String saveStudent(@ModelAttribute @Valid Student student, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("cities", cities);
			return "student-form";
		} else if (studentService.isEmailAlreadyInUse(student.getId(), student.getEmail(), student.getIndexNumber())) {
			model.addAttribute("emailError", "Email and index must be unique! Check your input");
			model.addAttribute("cities", cities);
			return "student-form";
		}
		studentService.save(student);
		return "redirect:/students";
	}
	
	@GetMapping("/{studentId}/update")
	public String showUpdateForm(@PathVariable Integer studentId, Model model) {
		Student student = studentService.findById(studentId);
		model.addAttribute("cities", cities);
		model.addAttribute("student", student);
		return "student-form";
	}
	
	@GetMapping("/{studentId}/delete")
	public String deleteSubject(@PathVariable int studentId) {
		studentService.delete(studentId);
		return "redirect:/students";
	}
	
	@GetMapping("/{studentId}/subject")
	public String addSubjectForm(@PathVariable Integer studentId, Model model) {
		Student student = studentService.findById(studentId);
		List<Subject> subjects = subjectService.findAll();
		subjects.removeAll(student.getSubjects());
		model.addAttribute("subjects",subjects);
		model.addAttribute("subject", new Subject());
		return "assign-subject";
	}
	
	@PostMapping("/{studentId}/subject")
	public String addSubjectToProfessor(@PathVariable Integer studentId, @ModelAttribute Subject subject) {
		Student student = studentService.findById(studentId);
		Subject sub = subjectService.fetchStudents(subject.getSubjectID());
		student.addSubject(sub);
		studentService.save(student);
		subjectService.save(sub);
		return "redirect:/students/"+studentId;
	}
	
}

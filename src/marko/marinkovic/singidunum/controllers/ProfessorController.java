package marko.marinkovic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import marko.marinkovic.singidunum.entities.City;
import marko.marinkovic.singidunum.entities.Professor;
import marko.marinkovic.singidunum.entities.Subject;
import marko.marinkovic.singidunum.entities.Title;
import marko.marinkovic.singidunum.services.CityService;
import marko.marinkovic.singidunum.services.ProfessorService;
import marko.marinkovic.singidunum.services.SubjectService;
import marko.marinkovic.singidunum.services.TitleService;

@Controller
@RequestMapping("/professors")
public class ProfessorController {

	private ProfessorService professorService;
	private SubjectService subjectService;
	private CityService cityService;
	private TitleService titleService;
	private List<City> cities;
	private List<Title> titles;
	
	@Autowired
	public ProfessorController(ProfessorService professorService, SubjectService subjectService, CityService cityService,
			TitleService titleService) {
		this.professorService = professorService;
		this.subjectService = subjectService;
		this.cityService = cityService;
		this.titleService = titleService;
		this.cities = cityService.findAll();
		this.titles = titleService.findAll();
	}

	@GetMapping("")
	public String getProfessors(Model model) {
		List<Professor> professors = professorService.findAll();
		model.addAttribute("professors", professors);
		return "professors";
	}
	
	@GetMapping("/addProfessorForm")
	public String showProfessorForm(Model model) {
		Professor professor = new Professor();
		model.addAttribute("professor", professor);
		model.addAttribute("titles", titles);
		model.addAttribute("cities", cities);
		return "professor-form";
	}
	
	@PostMapping("/saveProfessor")
	public String saveProfessor(@ModelAttribute @Valid Professor professor, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("titles", titles);
			model.addAttribute("cities", cities);
			return "professor-form";
		} else if (professorService.isEmailAlreadyInUse(professor.getId(), professor.getEmail())) {
			model.addAttribute("emailError", "There is already user with this email!");
			model.addAttribute("titles", titles);
			model.addAttribute("cities", cities);
			return "professor-form";
		}
		professorService.save(professor);
		return "redirect:/professors";
	}
	
	@GetMapping("/{professorId}")
	public String getProfessor(@PathVariable Integer professorId, Model model) {
		Professor professor = professorService.findById(professorId);
		model.addAttribute("professor", professor);
		return "professor-details";
	}
	
	@GetMapping("/{professorId}/update")
	public String showUpdateForm(@PathVariable Integer professorId, Model model) {
		Professor professor = professorService.findById(professorId);
		model.addAttribute("professor", professor);
		model.addAttribute("titles", titles);
		model.addAttribute("cities", cities);
		return "professor-form";
	}
	
	@GetMapping("/{professorId}/delete")
	public String deleteProfessor(@PathVariable int professorId) {
		professorService.delete(professorId);
		return "redirect:/professors";
	}
	
	@GetMapping("/{professorId}/subject")
	public String assignNewSubjectToProfessor(@PathVariable Integer professorId, Model model) {
		Professor professor = professorService.findById(professorId);
		List<Subject> subjects = subjectService.findAll();
		subjects.removeAll(professor.getSubjects());
		model.addAttribute("subjects",subjects);
		model.addAttribute("subject", new Subject());
		return "assign-subject";
	}
	
	@PostMapping("/{professorId}/subject")
	public String addSubjectToProfessor(@PathVariable Integer professorId, @ModelAttribute Subject subject) {
		Professor professor = professorService.findById(professorId);
		Subject sub = subjectService.fetchProfessors(subject.getSubjectID());
		professor.addSubject(sub);
		professorService.save(professor);
		subjectService.save(sub);
		return "redirect:/professors/"+professorId;
	}
}

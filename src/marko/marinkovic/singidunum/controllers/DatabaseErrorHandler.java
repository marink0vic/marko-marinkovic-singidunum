package marko.marinkovic.singidunum.controllers;

import org.hibernate.exception.DataException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DatabaseErrorHandler {

	@ExceptionHandler(DataException.class)
	public String handleDatabaseException(DataException e, Model model) {
		model.addAttribute("message", "Sorry problem accured while saving your data.Please check you input");
		return "error";
	}
	
	@ExceptionHandler(NumberFormatException.class)
	public String handleNumberFormatException(NumberFormatException e, Model model) {
		model.addAttribute("message", "You must enter number in your input field");
		return "error";
	}
	
}

package marko.marinkovic.singidunum.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import marko.marinkovic.singidunum.entities.Exam;
import marko.marinkovic.singidunum.entities.Student;
import marko.marinkovic.singidunum.entities.Subject;
import marko.marinkovic.singidunum.services.ExamService;
import marko.marinkovic.singidunum.services.StudentService;
import marko.marinkovic.singidunum.services.SubjectService;

@Controller
@RequestMapping("/exams")
public class ExamController {

	private ExamService examService;
	private SubjectService subjectService;
	private StudentService studentService;

	@Autowired
	public ExamController(ExamService examService, SubjectService subjectService, StudentService studentService) {
		this.examService = examService;
		this.subjectService = subjectService;
		this.studentService = studentService;
	}

	@GetMapping("")
	public String getExams(Model model) {
		List<Exam> exams = examService.findAll();
		model.addAttribute("exams", exams);
		return "exams";
	}
	
	@PostMapping("")
	public String getExamsWithLimit(Model model, HttpServletRequest request) {
		int size = Integer.parseInt(request.getParameter("examNumber"));
		List<Exam> exams = examService.findAllWithLimit(size);
		model.addAttribute("exams", exams);
		return "exams";
	}

	@PostMapping("/{subjectId}/create")
	public String createExam(@ModelAttribute @Valid Exam exam, BindingResult result, @PathVariable Integer subjectId, Model model) {
		Subject subject = subjectService.fetchProfessors(subjectId);
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("exam", exam);
			model.addAttribute("subject", subject);
			return "create-exam";
		}
		
		List<LocalDate> activeExamDates = examService.getActiveExamDates(exam);
		if (activeExamDates.isEmpty()) {
			examService.save(exam);
			return "redirect:/exams";
		} else {
			model.addAttribute("date_message", "Date for that exam is still active, choose another date");
			model.addAttribute("activeExamDates", activeExamDates);
			model.addAttribute("exam", exam);
			model.addAttribute("subject", subject);
			return "create-exam";
		}
	}
	
	@GetMapping("/{studentId}/subject/{subjectId}")
	public String showActiveExamDates(@PathVariable Integer studentId, @PathVariable Integer subjectId, Model model) {
		List<Exam> exams = examService.getExamsForSubject(subjectId);
		if (exams.size() == 0)
			return "no-active-exam";
		model.addAttribute("exams", exams);
		model.addAttribute("exam", new Exam());
		return "student-select-exam";
	}
	
	@PostMapping("/{studentId}/subject/{subjectId}")
	public String saveExamToStudent(@ModelAttribute Exam exam, @PathVariable Integer studentId, Model model) {
		Student student = studentService.fetchExams(studentId);
		exam = examService.findById(exam.getExamId());
		if (!(examNoExist(student.getExams(), exam))) {
			student.addExam(exam);
			studentService.save(student);
			examService.save(exam);
			return "redirect:/exams";
		} else {
			model.addAttribute("examId",exam.getExamId());
			return "duplicate-exam";
		}
	}
	
	private boolean examNoExist(Set<Exam> exams, Exam exam) {
		return exams.stream().anyMatch(e -> e.getExamId() == exam.getExamId());
	}

	@GetMapping("/{examId}/students")
	public String studentsAssignedForExam(@PathVariable int examId, Model model) {
		Exam exam = examService.findById(examId);
		model.addAttribute("exam", exam);
		return "exam-student-details";
	}
	
}

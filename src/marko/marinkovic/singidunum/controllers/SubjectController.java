package marko.marinkovic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import marko.marinkovic.singidunum.entities.Exam;
import marko.marinkovic.singidunum.entities.Subject;
import marko.marinkovic.singidunum.services.SubjectService;

@Controller
@RequestMapping("/subjects")
public class SubjectController {

	private SubjectService service;

	@Autowired
	public SubjectController(SubjectService service) {
		this.service = service;
	}
	
	@GetMapping("")
	public String getSubjects(Model model) {
		List<Subject> subjects = service.findAll();
		model.addAttribute("subjects", subjects);
		return "subjects";
	}
	
	@GetMapping("/addSubjectForm")
	public String showSubjectForm(Model model) {
		Subject subject = new Subject();
		model.addAttribute("subject", subject);
		return "subject-form";
	}
	
	@GetMapping("/{subjectId}")
	public String showUpdateForm(Model model, @PathVariable int subjectId) {
		Subject subject = service.findById(subjectId);
		model.addAttribute("subject", subject);
		return "subject-form";
	}
	
	@PostMapping("/saveSubject")
	public String saveSubject(@ModelAttribute @Valid Subject subject, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return "subject-form";
		}
		service.save(subject);
		return "redirect:/subjects";
	}
	
	@GetMapping("/{subjectId}/delete")
	public String deleteSubject(@PathVariable int subjectId) {
		service.delete(subjectId);
		return "redirect:/subjects";
	}
	
	@GetMapping("/{subjectId}/exam")
	public String createExam(@PathVariable int subjectId, Model model) {
		Subject subject = service.fetchProfessors(subjectId);
		if (subject.getProfessors().size() == 0)
			model.addAttribute("no_professor_message", "No professor assigned for this subject");
		model.addAttribute("subject", subject);
		model.addAttribute("exam", new Exam());
		return "create-exam";
	}
}

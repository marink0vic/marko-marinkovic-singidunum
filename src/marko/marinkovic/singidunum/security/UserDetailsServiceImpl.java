package marko.marinkovic.singidunum.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.ProfessorDao;
import marko.marinkovic.singidunum.entities.Professor;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private ProfessorDao professorDao;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Professor professor = professorDao.findByEmail(email);
		System.out.println(professor);
		if (professor == null)
			throw new UsernameNotFoundException("User does not exist");
		return professor;
	}

}

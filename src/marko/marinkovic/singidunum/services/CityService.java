package marko.marinkovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.CityDao;
import marko.marinkovic.singidunum.entities.City;

@Service
public class CityService implements SchoolCrudService<City> {
	
	private CityDao cityDao;

	@Autowired
	public CityService(CityDao cityDao) {
		this.cityDao = cityDao;
	}

	@Transactional
	@Override
	public void save(City city) {
		cityDao.save(city);
	}

	@Transactional
	@Override
	public List<City> findAll() {
		return cityDao.findAll();
	}

	@Transactional
	@Override
	public City findById(int id) {
		return cityDao.findById(id);
	}

	@Transactional
	@Override
	public void delete(int id) {
		cityDao.delete(id);
	}

}

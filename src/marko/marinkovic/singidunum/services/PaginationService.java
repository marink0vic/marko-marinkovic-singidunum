package marko.marinkovic.singidunum.services;

import java.util.List;

public interface PaginationService<T> {
	List<T> findAllWithLimit(int limit);
}

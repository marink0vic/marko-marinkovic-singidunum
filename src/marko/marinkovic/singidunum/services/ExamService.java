package marko.marinkovic.singidunum.services;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import static java.time.temporal.ChronoUnit.DAYS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.ExamDao;
import marko.marinkovic.singidunum.entities.Exam;

@Service
public class ExamService implements SchoolCrudService<Exam>, PaginationService<Exam> {
	
	private ExamDao examDao;
	
	@Autowired
	public ExamService(ExamDao examDao) {
		this.examDao = examDao;
	}

	@Transactional
	@Override
	public void save(Exam exam) {
		examDao.save(exam);
	}

	@Transactional
	@Override
	public List<Exam> findAll() {
		return examDao.findAll();
	}

	@Transactional
	@Override
	public Exam findById(int id) {
		return examDao.findById(id);
	}

	@Transactional
	@Override
	public void delete(int id) {
		examDao.delete(id);
	}
	
	@Transactional
	@Override
	public List<Exam> findAllWithLimit(int limit) {
		return examDao.findAllWithLimit(limit);
	}
	
	@Transactional
	public List<LocalDate> getActiveExamDates(Exam exam) {
		return examDao.findActiveForSubject(exam.getSubject().getSubjectID(), exam.getExamDate())
				.stream().map(Exam::getExamDate).collect(Collectors.toList());
	}

	@Transactional
	public List<Exam> getExamsForSubject(Integer subjectId) {
		List<Exam> exams = examDao.findActiveForSubject(subjectId, LocalDate.now());
		return exams.stream().filter(e -> DAYS.between(LocalDate.now(), e.getExamDate()) <= 7).collect(Collectors.toList());
	}

}

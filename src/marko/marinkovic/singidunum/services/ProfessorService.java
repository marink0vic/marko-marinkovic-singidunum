package marko.marinkovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.ProfessorDao;
import marko.marinkovic.singidunum.entities.Professor;

@Service
public class ProfessorService implements SchoolCrudService<Professor>{

	private ProfessorDao professorDao;

	@Autowired
	public ProfessorService(ProfessorDao professorDao) {
		this.professorDao = professorDao;
	}
	
	@Transactional
	@Override
	public void save(Professor professor) {
		if (professor.getId() == null) {
			PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			professor.setPassword(passwordEncoder.encode(professor.getPassword()));	
		}
		professorDao.save(professor);
	}
	
	@Transactional
	@Override
	public List<Professor> findAll() {
		return professorDao.findAll();
	}
	
	@Transactional
	@Override
	public Professor findById(int id) {
		return professorDao.findById(id);
	}
	
	@Transactional
	@Override
	public void delete(int id) {
		professorDao.delete(id);
	}
	
	@Transactional
	public boolean isEmailAlreadyInUse(Integer id, String email){
		boolean userInDb = true;
		if (id == null) id = 0;
		if (professorDao.getActiveProfessor(id, email) == null) 
			userInDb = false;
		return userInDb;
	}
}

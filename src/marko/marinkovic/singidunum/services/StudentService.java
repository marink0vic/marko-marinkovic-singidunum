package marko.marinkovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.StudentDao;
import marko.marinkovic.singidunum.entities.Student;

@Service
public class StudentService implements SchoolCrudService<Student>, PaginationService<Student> {

	private StudentDao studentDao;

	@Autowired
	public StudentService(StudentDao studentDao) {
		this.studentDao = studentDao;
	}

	@Transactional
	@Override
	public void save(Student student) {
		if (student.getId() == null) {
			PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			student.setPassword(passwordEncoder.encode(student.getPassword()));
		}
		studentDao.save(student);
	}

	@Transactional
	@Override
	public List<Student> findAll() {
		return studentDao.findAll();
	}

	@Transactional
	@Override
	public Student findById(int id) {
		return studentDao.findById(id);
	}

	@Transactional
	@Override
	public void delete(int id) {
		studentDao.delete(id);
	}

	@Transactional
	public Student fetchExams(Integer studentId) {
		return studentDao.fetchExams(studentId);
	}

	@Transactional
	public Student fetchAll(Integer studentId) {
		return studentDao.fetchAll(studentId);
	}

	@Transactional
	@Override
	public List<Student> findAllWithLimit(int limit) {
		return studentDao.findAllWithLimit(limit);
	}
	
	@Transactional
	public boolean isEmailAlreadyInUse(Integer id, String email, String index){
		boolean userInDb = true;
		if (id == null) id = 0;
		if (studentDao.getActiveStudent(id, email, index) == null) 
			userInDb = false;
		return userInDb;
	}
	
}

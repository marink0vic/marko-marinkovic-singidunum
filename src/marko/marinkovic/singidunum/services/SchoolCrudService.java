package marko.marinkovic.singidunum.services;

import java.util.List;

public interface SchoolCrudService<T> {
	void save(T object);
	List<T> findAll();
	T findById(int id);
	void delete(int id);
}

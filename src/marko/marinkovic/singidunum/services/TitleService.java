package marko.marinkovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.TitleDao;
import marko.marinkovic.singidunum.entities.Title;

@Service
public class TitleService implements SchoolCrudService<Title>{

	private TitleDao titleDao;

	@Autowired
	public TitleService(TitleDao titleDao) {
		this.titleDao = titleDao;
	}

	@Transactional
	@Override
	public void save(Title title) {
		titleDao.save(title);
	}

	@Transactional
	@Override
	public List<Title> findAll() {
		return titleDao.findAll();
	}

	@Transactional
	@Override
	public Title findById(int id) {
		return titleDao.findById(id);
	}

	@Transactional
	@Override
	public void delete(int id) {
		titleDao.delete(id);
	}
}

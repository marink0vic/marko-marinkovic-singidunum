package marko.marinkovic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import marko.marinkovic.singidunum.dao.SubjectDao;
import marko.marinkovic.singidunum.entities.Subject;

@Service
public class SubjectService implements SchoolCrudService<Subject>{

	private SubjectDao subjectDao;

	@Autowired
	public SubjectService(SubjectDao subjectDao) {
		this.subjectDao = subjectDao;
	}
	
	@Transactional
	@Override
	public void save(Subject subject) {
		subjectDao.save(subject);
	}
	
	@Transactional
	@Override
	public List<Subject> findAll() {
		return subjectDao.findAll();
	}
	
	@Transactional
	@Override
	public Subject findById(int id) {
		return subjectDao.findById(id);
	}
	
	@Transactional
	@Override
	public void delete(int id) {
		subjectDao.delete(id);
	}
	
	@Transactional
	public Subject fetchStudents(int id) {
		return subjectDao.fetchStudents(id);
	}
	
	@Transactional
	public Subject fetchProfessors(int id) {
		return subjectDao.fetchProfessors(id);
	}
}

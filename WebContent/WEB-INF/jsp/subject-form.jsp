<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<div class="container">
<c:forEach var="error" items="${errors}">
	<div class="alert alert-danger" role="alert">
  		<p><c:out value="${error.getDefaultMessage()}"></c:out></p>
	</div>
</c:forEach>
<h2>Add Subject</h2>
<form:form action="saveSubject" modelAttribute="subject" method="post">
  <form:hidden path="subjectID"/>
  <div class="form-group">
    <label for="subjectName">Name</label>
    <form:input type="text" class="form-control" id="subjectName" path="name"/>
  </div>
  <div class="form-group">
    <label for="subjectDesc">Description</label>
    <form:input type="text" class="form-control" id="subjectDesc" path="description"/>
  </div>
  <div class="form-group">
      <label for="inputYear">Year of Study</label>
      <form:select id="inputYear" type="number" class="form-control" path="yearOfStudy">
      <c:choose>
	      <c:when test="${empty subject.subjectID}">
	        <option selected>1</option>
	        <option>2</option>
	        <option>3</option>
	        <option>4</option>    
	      </c:when>
	      <c:otherwise>
	        <c:forEach var = "i" begin = "1" end = "4">
		        <c:choose>
		        	<c:when test="${subject.yearOfStudy == i}">
		        	  <option selected><c:out value = "${i}"/></option>
		        	</c:when>
		        	<c:otherwise>
		        	  <option><c:out value = "${i}"/></option>
		        	</c:otherwise>
		        </c:choose>
		     </c:forEach>
    	  </c:otherwise>
      </c:choose>
      </form:select>
   </div>
   <div class="form-group">
      <label for="inputSemester">Semester</label>
      <form:select id="inputSemester" class="form-control" path="semester">
      	<c:choose>
      		<c:when test="${empty subject.subjectID}">
      			<option selected>SUMMER</option>
        		<option>WINTER</option>
      		</c:when>
      		<c:otherwise>
      			<c:if test="${subject.semester eq 'SUMMER'}">
      				<option selected>SUMMER</option>
        			<option>WINTER</option>
				</c:if>
				<c:if test="${subject.semester eq 'WINTER'}">
        			<option selected>WINTER</option>
      				<option>SUMMER</option>
				</c:if>
      		</c:otherwise>
      	</c:choose>
      </form:select>
   </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form:form>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
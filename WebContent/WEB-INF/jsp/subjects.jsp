<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Subjects Page</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<h1>Subject page</h1>
<hr></hr>
<button type="button" class="btn btn-primary" onclick="window.location.href='${pageContext.request.contextPath}/subjects/addSubjectForm'">Add Subject</button>
<br></br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Year of Study</th>
      <th scope="col">Semester</th>
      <th scope="col">Create Exam</th>
      <th scope="col">Update</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach var="subject" items="${subjects}">
  	<input type="hidden" value="${subject.subjectID}">
    <tr>
      <td><c:out value="${subject.name}"/></td>
      <td><c:out value="${subject.description}"/></td>
      <td><c:out value="${subject.yearOfStudy}"/></td>
      <td><c:out value="${subject.semester}"/></td>
      <td>
      	<a href="${pageContext.request.contextPath}/subjects/${subject.subjectID}/exam">Create Exam</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/subjects/${subject.subjectID}">Update</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/subjects/${subject.subjectID}/delete"
      	onclick="if(!(confirm('Are you sure you want to delete subject?'))) return false">Delete</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
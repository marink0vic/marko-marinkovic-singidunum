<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<div class="container">
<c:forEach var="error" items="${errors}">
	<div class="alert alert-danger" role="alert">
  		<p><c:out value="${error.getDefaultMessage()}"></c:out></p>
	</div>
</c:forEach>
<c:if test="${not empty emailError}">
	<div class="alert alert-danger" role="alert">
  		<p><c:out value="${emailError}"></c:out></p>
	</div>
</c:if>
<h2>Add Professor</h2>
<form:form action="${pageContext.request.contextPath}/professors/saveProfessor" modelAttribute="professor" method="post">
  <form:hidden path="id"/>
  <div class="form-group">
    <label for="firstName">Name</label>
    <form:input type="text" class="form-control" id="firstName" path="firstName"/>
  </div>
  <div class="form-group">
    <label for="lastName">Surname</label>
    <form:input type="text" class="form-control" id="lastName" path="lastName"/>
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <form:input type="email" class="form-control" id="email" path="email"/>
  </div>
  <c:choose>
  <c:when test="${empty professor.id}">
	  <div class="form-group">
	    <label for="password">Password</label>
	    <form:input type="password" class="form-control" id="password" path="password"/>
	  </div>
  </c:when>
  <c:otherwise>
  	<form:hidden path="password"/>
  </c:otherwise>
  </c:choose>
  <div class="form-group">
    <label for="address">Address</label>
    <form:input type="text" class="form-control" id="address" path="address"/>
  </div>
  <div class="form-group">
    <label for="phone">Phone</label>
    <form:input type="text" class="form-control" id="phone" path="phone"/>
  </div>
  <div class="form-group">
    <label for="reelectionDate">Reelection Date</label>
    <form:input type="text" class="form-control" id="reelectionDate" path="reelectionDate"/> 
  </div>
  <div class="form-group">
	<label for="titleLabel">Title</label>
	<form:select id="titleLabel" class="form-control" path="title.titleId">
	<c:choose>
		<c:when test="${empty professor.id}">
			<c:forEach var="title" items="${titles}">
		   		<option value="${title.titleId}">${title.name}</option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<c:forEach var="title" items="${titles}">
				<c:choose>
		        	<c:when test="${professor.title.titleId == title.titleId}">
		        	  <option selected value="${title.titleId}">${title.name}</option>
		        	</c:when>
		        	<c:otherwise>
		        	  <option value="${title.titleId}">${title.name}</option>
		        	</c:otherwise>
		        </c:choose>
			</c:forEach>
		</c:otherwise>
	</c:choose>
	</form:select>
  </div>
  <div class="form-group">
	<label for="cityLabel">City</label>
	<form:select id="cityLabel" class="form-control" path="city.cityId">
	<c:choose>
		<c:when test="${empty professor.id}">
			<c:forEach var="city" items="${cities}">
		   		<option value="${city.cityId}">${city.name}</option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<c:forEach var="city" items="${cities}">
				<c:choose>
		        	<c:when test="${professor.city.cityId == city.cityId}">
		        	  <option selected value="${city.cityId}">${city.name}</option>
		        	</c:when>
		        	<c:otherwise>
		        	  <option value="${city.cityId}">${city.name}</option>
		        	</c:otherwise>
		        </c:choose>
			</c:forEach>
		</c:otherwise>
	</c:choose>
	</form:select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form:form>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
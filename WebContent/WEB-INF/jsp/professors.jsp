<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Professors Page</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<h1>Professors page</h1>
<hr></hr>
<button type="button" class="btn btn-primary" onclick="window.location.href='${pageContext.request.contextPath}/professors/addProfessorForm'">Add new professor</button>
<br></br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Address</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Title</th>
      <th scope="col">Details</th>
      <th scope="col">Update</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach var="professor" items="${professors}">
  	<input type="hidden" value="${professor.id}">
    <tr>
      <td><c:out value="${professor.firstName}"/></td>
      <td><c:out value="${professor.lastName}"/></td>
      <td><c:out value="${professor.address}"/></td>
      <td><c:out value="${professor.phone}"/></td>
      <td><c:out value="${professor.title.name}"/></td>
      <td>
      	<a href="${pageContext.request.contextPath}/professors/${professor.id}">Professor info</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/professors/${professor.id}/update">Update</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/professors/${professor.id}/delete"
      	 onclick="if(!(confirm('Are you sure you want to delete professor?'))) return false">Delete</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
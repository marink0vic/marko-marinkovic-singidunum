<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student details page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<script type="text/javascript">
	function disableButton(subjectYear, studentYear) {
		if (subjectYear > studentYear)
			document.getElementById("year-"+subjectYear).classList.add("disabled", "btn-secondary");
	}
</script>
<c:import url="navigation.jsp"/>
<div class="container">
<div class="card-group">
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Student Details
	  </div>
	  <ul class="list-group list-group-flush">
	    <li class="list-group-item">Name: ${student.firstName}</li>
	    <li class="list-group-item">Surname: ${student.lastName}</li>
	    <li class="list-group-item">Email: ${student.email}</li>
	    <li class="list-group-item">Address: ${student.address}</li>
	    <li class="list-group-item">Phone: ${student.phone}</li>
	    <li class="list-group-item">Index Number: ${student.indexNumber}</li>
	    <li class="list-group-item">Current Year of study: ${student.currentStudyYear}</li>
	    <li class="list-group-item">City: ${student.city.name}</li>
	  </ul>
	</div>
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Student Subjects
	  </div>
	  <ul class="list-group list-group-flush">
	  	<c:forEach var="subject" items="${student.subjects}">
	  		<li class="list-group-item">${subject.name}, year: ${subject.yearOfStudy}
	  		| 
	  		<a id="year-${subject.yearOfStudy}" href="${pageContext.request.contextPath}/exams/${student.id}/subject/${subject.subjectID}" class="btn btn-primary stretched-link btn-sm">Apply for exam</a>
	  		</li>
	  		<script>disableButton("${subject.yearOfStudy}", "${student.currentStudyYear}")</script>
	  	</c:forEach>
	  </ul>
	</div>
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
		 Student Applied Exams
	  </div>
	  <ul class="list-group list-group-flush">
	  	<c:forEach var="exam" items="${student.exams}">
	  		<li class="list-group-item">${exam.subject.name}, date: ${exam.examDate}
	  		</li>
	  	</c:forEach>
	  </ul>
	</div>
</div>
<br></br>
<a href="${pageContext.request.contextPath}/students" class="btn btn-primary stretched-link">Return to students</a>
<a href="${pageContext.request.contextPath}/students/${student.id}/subject" class="btn btn-primary stretched-link">Add Subject</a>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>exam-students</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<c:import url="navigation.jsp"/>
<div class="container">
<div class="card-group">
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Exam Details
	  </div>
	  <ul class="list-group list-group-flush">
	    <li class="list-group-item">Exam: ${exam.subject.name}</li>
	    <li class="list-group-item">Professor name: ${exam.professor.firstName} ${exam.professor.lastName}</li>
	    <li class="list-group-item">Date: ${exam.examDate}</li>
	    <li class="list-group-item">Status: ${exam.status}</li>
	  </ul>
	</div>
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Exam Students
	  </div>
	  <ul class="list-group list-group-flush">
	  	<c:forEach var="student" items="${exam.students}">
	  		<li class="list-group-item">${student.firstName} ${student.lastName} index: ${student.indexNumber}</li>
	  	</c:forEach>
	  </ul>
	</div>
</div>
<br></br>
<a href="${pageContext.request.contextPath}/exams" class="btn btn-primary stretched-link">Return to exams</a>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Exams Page</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<h1>Exams page</h1>
<hr></hr>
<button type="button" class="btn btn-primary" onclick="window.location.href='${pageContext.request.contextPath}/subjects'">Assign exam date for subjects</button>
<br></br>
<form action="${pageContext.request.contextPath}/exams" method="post">
  <p>Select number of exams you want to see</p>
  <div class="form-row align-items-center">
    <div class="col-auto">
      <input type="number" class="form-control mb-2" name="examNumber">
    </div>
    <div class="col-auto">
      <button type="submit" class="btn btn-primary mb-2">Search</button>
    </div>
  </div>
</form>
<br></br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Professor</th>
      <th scope="col">Subject</th>
      <th scope="col">Date</th>
      <th scope="col">Status</th>
      <th scope="col">Exam Details</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach var="exam" items="${exams}">
  	<input type="hidden" value="${exam.examId}">
    <tr>
      <td><c:out value="${exam.professor.firstName} ${exam.professor.lastName}"/></td>
      <td><c:out value="${exam.subject.name}"/></td>
      <td><c:out value="${exam.examDate}"/></td>
      <td><c:out value="${exam.status}"/></td>
      <td>
      	<a href="${pageContext.request.contextPath}/exams/${exam.examId}/students">Exam Details</a>
      </td>
      <td>
    </tr>
    </c:forEach>
  </tbody>
</table>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
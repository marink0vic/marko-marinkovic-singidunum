<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Students Page</title>
</head>
<body>
<c:import url="navigation.jsp"/>
<h1>Students page</h1>
<hr></hr>
<button type="button" class="btn btn-primary" onclick="window.location.href='${pageContext.request.contextPath}/students/addStudentForm'">Add new student</button>
<br></br>
<form action="${pageContext.request.contextPath}/students" method="post">
  <p>Select number of student you want to see</p>
  <div class="form-row align-items-center">
    <div class="col-auto">
      <input type="number" class="form-control mb-2" name="studentNumber">
    </div>
    <div class="col-auto">
      <button type="submit" class="btn btn-primary mb-2">Search</button>
    </div>
  </div>
</form>
<br></br>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Address</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Index Number</th>
      <th scope="col">Details</th>
      <th scope="col">Update</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach var="student" items="${students}">
  	<input type="hidden" value="${student.id}">
    <tr>
      <td><c:out value="${student.firstName}"/></td>
      <td><c:out value="${student.lastName}"/></td>
      <td><c:out value="${student.address}"/></td>
      <td><c:out value="${student.phone}"/></td>
      <td><c:out value="${student.indexNumber}"/></td>
      <td>
      	<a href="${pageContext.request.contextPath}/students/${student.id}">Student info</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/students/${student.id}/update">Update</a>
      </td>
      <td>
      	<a href="${pageContext.request.contextPath}/students/${student.id}/delete"
      	 onclick="if(!(confirm('Are you sure you want to delete student?'))) return false">Delete</a>
      </td>
    </tr>
    </c:forEach>
  </tbody>
</table>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>create exam</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<c:import url="navigation.jsp"/>
<div class="container">
<h1>Create Exam</h1>
<c:forEach var="error" items="${errors}">
	<div class="alert alert-danger" role="alert">
  		<p><c:out value="${error.getDefaultMessage()}"></c:out></p>
	</div>
</c:forEach>
<c:if test="${not empty no_professor_message}">
	<div class="alert alert-danger" role="alert">
	  <p>${no_professor_message}</p>
	</div>
</c:if>
<c:if test="${not empty date_message}">
	<div class="alert alert-danger" role="alert">
	  <p>${date_message}</p>
	  <c:forEach var="date" items="${activeExamDates}">
	  	<p><c:out value="Exam assign for date: ${date}"/></p>
	  </c:forEach>
	</div>
</c:if>
<form:form action="${pageContext.request.contextPath}/exams/${subject.subjectID}/create" modelAttribute="exam" method="post">
  <div class="form-group">
  	<form:hidden value="${subject.subjectID}" path="subject.subjectID"/>
  	<form:hidden value="${subject.name}" path="subject.name"/>
    <label for="subjectName">Subject </label>
    <form:input type="text" class="form-control" value="${subject.name}" id="subjectName" path="subject.name" disabled="true"/>
  </div>
  <div class="form-group">
	<label for="professorLabel">Choose Professor</label>
	<form:select id="professorLabel" class="form-control" path="professor.id">
	   <c:forEach var="prof" items="${subject.professors}">
	      <option value="${prof.id}">${prof.firstName} ${prof.lastName}</option>
	   </c:forEach>
	</form:select>
  </div>
  <div class="form-group">
    <label for="date">Exam Date</label>
    <form:input type="text" class="form-control" id="date" path="examDate"/>
  </div>
  <c:if test="${empty no_professor_message}">
	<button type="submit" class="btn btn-primary">Submit</button>
  </c:if>
</form:form>
<c:if test="${not empty no_professor_message}">
	<button type="button" class="btn btn-primary" onclick="window.location.href='${pageContext.request.contextPath}/subjects'">Return to subjects page</button>
</c:if>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Professor detail page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<c:import url="navigation.jsp"/>
<div class="container">
<div class="card-group">
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Professor Details
	  </div>
	  <ul class="list-group list-group-flush">
	    <li class="list-group-item">Name: ${professor.firstName}</li>
	    <li class="list-group-item">Surname: ${professor.lastName}</li>
	    <li class="list-group-item">Email: ${professor.email}</li>
	    <li class="list-group-item">Address: ${professor.address}</li>
	    <li class="list-group-item">Phone: ${professor.phone}</li>
	    <li class="list-group-item">Reelection Date: ${professor.reelectionDate}</li>
	    <li class="list-group-item">City: ${professor.city.name}</li>
	    <li class="list-group-item">Title: ${professor.title.name}</li>
	  </ul>
	</div>
	<div class="card" style="width: 10rem;">
	  <div class="card-header">
	    Professor Subjects
	  </div>
	  <ul class="list-group list-group-flush">
	  	<c:forEach var="subject" items="${professor.subjects}">
	  		<li class="list-group-item">${subject.name}</li>
	  	</c:forEach>
	  </ul>
	</div>
</div>
<br></br>
<a href="${pageContext.request.contextPath}/professors" class="btn btn-primary stretched-link">Return to professors</a>
<a href="${pageContext.request.contextPath}/professors/${professor.id}/subject" class="btn btn-primary stretched-link">Add Subject</a>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
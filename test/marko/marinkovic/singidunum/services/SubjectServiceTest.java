package marko.marinkovic.singidunum.services;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import marko.marinkovic.singidunum.dao.SubjectDao;
import marko.marinkovic.singidunum.entities.Subject;


public class SubjectServiceTest {
	
	private SubjectService service;
	private SubjectDao subjectDao;
	private Subject subject;
	
	@Before
	public void setup() {
		subjectDao = mock(SubjectDao.class);
		service = new SubjectService(subjectDao);
		subject = new Subject();
		subject.setSubjectID(1);
		subject.setName("Math");
	}

	@Test
	public void findAllSubjects() {
		Subject s2 = new Subject();
		s2.setSubjectID(2);
		s2.setName("English");
		
		when(subjectDao.findAll()).thenReturn(Arrays.asList(subject, s2));
		List<Subject> list = service.findAll();
		
		assertEquals(2, list.size());
		verify(subjectDao, times(1)).findAll();
	}
	
	@Test
	public void findByIdSuccess() {
		when(subjectDao.findById(anyInt())).thenReturn(subject);
		Subject temp = service.findById(1);
		assertEquals("Subject id numbers should be equal but there are not",subject.getSubjectID(), temp.getSubjectID());
		verify(subjectDao,times(1)).findById(anyInt());
	}
	
	@Test
	public void findByIdNotFound() {
		when(subjectDao.findById(anyInt())).thenReturn(null);
		Subject temp = service.findById(1);
		assertNotEquals("Subject is found but it shouldn't be",subject, temp);
	}
	
}
